import { Data } from "../Chat";

export let mock: Data = {
    id: "",
    user: "Daniel",
    avatar: "https://thispersondoesnotexist.com/image",
    userId: "2c34b119-eda4-4dfa-84b8-27bf1a18b0d9",
    editedAt: "",
    text: "",
    createdAt: ""
};