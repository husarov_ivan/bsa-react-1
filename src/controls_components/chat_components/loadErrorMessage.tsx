import { Data } from "../Chat";

export let errorData: Data = {
    id: "",
    user: "Error",
    avatar: "",
    userId: "---",
    editedAt: "",
    text: "If you see this message, it is an error to load data. Try again later.",
    createdAt: ""
};